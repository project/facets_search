<?php

namespace Drupal\facets_search\Plugin\facets\widget;

use Drupal\facets\Plugin\facets\widget\CheckboxWidget;

/**
 * The checkbox / radios widget with a textfield search.
 *
 * @FacetsWidget(
 *   id = "facet_textfield_search",
 *   label = @Translation("List of checkbox with a textfield search"),
 *   description = @Translation("A textfield to search among terms."),
 * )
 */
class FacetTextfieldSearch extends CheckboxWidget {

  /**
   * {@inheritdoc}
   */
  protected function appendWidgetLibrary(array &$build) {
    parent::appendWidgetLibrary($build);
    $build['#attributes']['class'][] = 'js-facets-checkbox-links';
    $build['#attached']['library'][] = 'facets_search/facets_search';
  }

}
